# Endo
*Endo* is a code documentation system that focuses on being light and with ubiquity. Endo allows users to document code by using a special set of flags inside the comments of a file to describe different features of the code. Users can then search through the code and find occurences of function, find functions by description, name, return value or other distinctive features. Endo can also map functions, which means that every call to a function is traced to the header it was defined or the file it was defined in.

## How to use the Special Comment Flags
Since *Endo* is relatively new, only the C and C++ languages are supported. The only structure so far supported are functions. Here is how you setup a group of flags for a function. First, you must make sure that the block precedes the definition and does not have any gaps between itself and the function. Here is an example:
```
//--------------------------------------------------
//@Type: Function
//@Name: main
//@Desc: The Main function of this program.
//@Args: argc : integer, argv : character[] 
//@Retn: int
//@Call: printf : <stdio.h>, print_hello, print_argc
//--------------------------------------------------
int main (int argc, char *argv)
{
	print_argc(argc);
	printf("%s", print_hello());
}
```
The first thing you will notice is the repeating structure of "//@{Flag}:", this is important to note because it allows Endo to parse the block and understand the function. The first flag is used to determine the type of structure being documented. This does not affect much right now, but it will be important in later updates with the integration of structures, headers, unions, et cetera. The second flag is the name, which is simply the same name used in the function. The third flag is the description flag, which allows the programmer to set a brief description of what this functions job is and other relevant details that someone looking for the function might like to know. The fourth flag is the argument flag, which is responsible for describing the arguments and their types that are used by the function. Since both the name and type is listed, you can search for both in the argument flag. The fifth flag is the return value flag, which tells what the functions return value is. The sixth and final flag is the call flag, which names funcitons that are referenced within this functions body. This flag is particularly useful for "tracing" function and finding what calls are made by each function.

## Command Line Arguments

### -h, --help

This command line flag triggers the usage and help menu.

### -n FUNC

This command line flag takes a functions name and parses down the given directory for functions conforming to the query.

### -d DESC

This command line flag takes a description, or a substring from a description, and parses down the given directory for the query.

### -a ARGS

This command line flag takes both the arguments names and/or types and parses down the given directory to find function conforming to the given query.

### -r RETN

This command line flag takes the return value and searchs for functions returning the same value.

### -c CALL

This command line flag takes any functions that you want to find out that get referenced. This is particularly useful if you know a defined function and you want to see in what places is it used.

### -f FOLDER

This command designates a folder and all its sub directories to be walked by the parser.

### -t

This flag enables "trace" mode and will trace functions references.

### -v

Toggles verbose mode, this function is used mostly to check command line arguments and trigger any debug messages.

## Running

### Linux

Navigate to the script or place it in your $PATH. To execute make sure you have sufficient permissions and type the command:

```
; ./endo {flags}

or

; python endo {flags}
```

### Windows

Use python and run it inside the fake terminal or whatever is used to simulate a working computer. If you need to, buil d a binary using cx_Freeze and execute from CMD.exe.

## Building Binaries

For some reason if you feel the need to have binaries, you can use the program `cx_Freeze` to create a frozen version of Endo. Note that this only targets the platform you are freezing on and can have some problems in the process. Make sure all needed libraries and dependencies are installed and that you have a working python installation before proceeding.

*Note: There is no support for bugs with frozen versions, as they are highly experimental.*
